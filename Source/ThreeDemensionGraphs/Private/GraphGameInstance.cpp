// Fill out your copyright notice in the Description page of Project Settings.

#include "GraphGameInstance.h"
#include "BaseNode.h"
#include "GraphBaseNode.h"
#include "ObjectMatcherActorComponent.h"
class UObjectMatcherActorComponent;
class BaseNode;
class AGraphBaseNode;

GraphGameInstance::GraphGameInstance()
{
}

GraphGameInstance::~GraphGameInstance()
{
}

void GraphGameInstance::StartGame()
{

}

void GraphGameInstance::SetObjectMatcherActorComponent(UObjectMatcherActorComponent* NewObjectMatcherActorComponent)
{
    ObjectMatcherActorComponent = NewObjectMatcherActorComponent;
}

BaseNode* GraphGameInstance::GetHoveredNode()
{
    AGraphBaseNode* GraphBaseNode = ObjectMatcherActorComponent->GetHoveredGraphBaseNode();
    if (GraphBaseNode) {
        return GraphBaseNode->GetBaseNode();
    } else {
        return nullptr;
    }
}

void GraphGameInstance::SetButtonsActions(ButtonsActions* NewGameButtonsActions)
{
    GameButtonsActions = NewGameButtonsActions;
}

ButtonsActions* GraphGameInstance::GetButtonsActions()
{
    return GameButtonsActions;
}
