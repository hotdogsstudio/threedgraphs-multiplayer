// Fill out your copyright notice in the Description page of Project Settings.

#include "GraphGameExecutor.h"
#include "JsonGraphGame.h"
#include "GraphGameInstance.h"

GraphGameExecutor::GraphGameExecutor()
{
}

GraphGameExecutor::~GraphGameExecutor()
{
}

GraphGameInstance* GraphGameExecutor::GetGameInstance()
{
	return new JsonGraphGame();
}
