// Fill out your copyright notice in the Description page of Project Settings.

#include "JsonGraphGame.h"
#include "SphereNode.h"
#include "LineNode.h"
#include "Json.h"
#include "Paths.h"
#include "FileHelper.h"
#include "JsonButtonsActions.h"

JsonGraphGame::JsonGraphGame()
{
    JsonButtonsActions* Actions = new JsonButtonsActions();
    Actions->GraphGame = this;
    SetButtonsActions(Actions);
}

JsonGraphGame::~JsonGraphGame()
{
}

void JsonGraphGame::StartGame()
{
	FString CompleteFilePath = FPaths::ConvertRelativePathToFull(FPaths::GameContentDir()) + "TextDocument/Test.txt";

	FString json;
	FFileHelper::LoadFileToString(json, *CompleteFilePath);


	TSharedPtr<FJsonObject> JsonParsed = MakeShareable(new FJsonObject());
	TSharedRef<TJsonReader<>> JsonReader = TJsonReaderFactory<>::Create(json);
	if (FJsonSerializer::Deserialize(JsonReader, JsonParsed) && JsonParsed.IsValid())
	{

		TArray<TSharedPtr<FJsonValue>> vertex = JsonParsed->GetArrayField("vertex");
		TArray<TSharedPtr<FJsonValue>> edges = JsonParsed->GetArrayField("edges");

		for (int32 i = 0; i < vertex.Num(); i++)
		{
			const TSharedPtr<FJsonObject>& oneVertex = vertex[i]->AsObject();
			Position position;
			position.x = oneVertex->GetNumberField("x");
			position.y = oneVertex->GetNumberField("y");
			position.z = oneVertex->GetNumberField("z");
			Color color = IntToColor(oneVertex->GetIntegerField("color"));
			FString nativeString = oneVertex->GetStringField("text");
			std::string title = FStringToStdString(nativeString);

			SphereNode* sphereN = new SphereNode();
			sphereN->SetColor(color);
			sphereN->SetPosition(position);
			sphereN->SetTitle(title);
			sphereN->SetContent(title);
			sphereN->Spawn();
		}

		for (int32 i = 0; i < edges.Num(); i++)
		{
			const TSharedPtr<FJsonObject>& oneEdge = edges[i]->AsObject();
			const TSharedPtr<FJsonObject>& objectPos1 = oneEdge->GetObjectField("id1");
			const TSharedPtr<FJsonObject>& objectPos2 = oneEdge->GetObjectField("id2");

			Position positionFrom;
			positionFrom.x = objectPos1->GetNumberField("x");
			positionFrom.y = objectPos1->GetNumberField("y");
			positionFrom.z = objectPos1->GetNumberField("z");

			Position positionTo;
			positionTo.x = objectPos2->GetNumberField("x");
			positionTo.y = objectPos2->GetNumberField("y");
			positionTo.z = objectPos2->GetNumberField("z");

			Color color = IntToColor(FCString::Atoi(*oneEdge->GetStringField("color")));
			std::string title = FStringToStdString(oneEdge->GetStringField("text"));

			LineNode* lineN = new LineNode();
			lineN->SetColor(color);
			lineN->SetPosition(positionFrom, positionTo);
			lineN->SetTitle(title);
			lineN->SetContent(title);
			lineN->Spawn();
		}
	}
}

Color JsonGraphGame::IntToColor(int intColor) {
	float mainColor = (intColor % 17) / 16.0f;
	Color color;
	color.r = 0;
	color.g = 0;
	color.b = 0;
	color.a = 1;
	switch (intColor % 3) {
	case 0:
		color.r = mainColor;
		break;
	case 1:
		color.g = mainColor;
		break;
	default:
		color.b = mainColor;
		break;
	}
	return color;
}

std::string JsonGraphGame::FStringToStdString(FString fstring) {
	return TCHAR_TO_UTF8(*fstring);
}

