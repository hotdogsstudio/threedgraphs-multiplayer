// Fill out your copyright notice in the Description page of Project Settings.

#include "GraphLineNode.h"
#include "Classes/Components/CapsuleComponent.h"
#include "Net/UnrealNetwork.h"


AGraphLineNode::AGraphLineNode()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Origin = CreateDefaultSubobject<USceneComponent>(TEXT("Origin"));
	RootComponent = Origin;

	CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule"));
	CapsuleComp->SetupAttachment(RootComponent);
	CapsuleComp->SetCapsuleRadius(20.0f, false);
	CapsuleComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CapsuleComp->SetCollisionObjectType(ECC_WorldDynamic);
	CapsuleComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	CapsuleComp->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Block);
}

void AGraphLineNode::OnChangePositionEvent_Implementation(){}

void AGraphLineNode::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGraphLineNode, startPosition);
	DOREPLIFETIME(AGraphLineNode, endPosition);
}