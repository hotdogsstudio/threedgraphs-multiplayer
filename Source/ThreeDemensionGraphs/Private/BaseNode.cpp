// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseNode.h"
#include "ObjectMatcherActorComponent.h"
#include "CoreTypes.h"




FLinearColor BaseNode::CastColorToFColor(Color color)
{
	FLinearColor fcolor;
	fcolor.R = color.r;
	fcolor.G = color.g;
	fcolor.B = color.b;
	fcolor.A = color.a;
	return fcolor;		
}

FVector BaseNode::CastPositionToFVector(Position position)
{
	FVector vector;
	vector.X = position.x;
	vector.Y = position.y;
	vector.Z = position.z;
	return vector;
}

BaseNode::BaseNode()
{
}
BaseNode::~BaseNode()
{

}

UObjectMatcherActorComponent* BaseNode::component = NULL;

void BaseNode::Spawn()
{

}

void BaseNode::SetTitle(std::string newTitle)
{
	title = newTitle;
}

void BaseNode::SetContent(std::string newContent)
{
	content = newContent;
}

std::string BaseNode::GetTitle()
{
	return title;
}

std::string BaseNode::GetContent()
{
	return content;
}


