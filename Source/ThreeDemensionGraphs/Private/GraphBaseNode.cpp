// Fill out your copyright notice in the Description page of Project Settings.

#include "GraphBaseNode.h"
#include "UnrealString.h"
#include "Net/UnrealNetwork.h"


// Sets default values
AGraphBaseNode::AGraphBaseNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;
}

// Called when the game starts or when spawned
void AGraphBaseNode::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AGraphBaseNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AGraphBaseNode::SetBaseNode(BaseNode* newBaseNode)
{
	baseNode = newBaseNode;
}

FString AGraphBaseNode::GetTitle()
{
	return baseNode->GetTitle().c_str();
}

FString AGraphBaseNode::GetContent()
{
	return baseNode->GetContent().c_str();
}

BaseNode* AGraphBaseNode::GetBaseNode()
{
    return baseNode;
}

void AGraphBaseNode::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AGraphBaseNode, color);
}