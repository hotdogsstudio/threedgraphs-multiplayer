// Fill out your copyright notice in the Description page of Project Settings.

#include "ObjectMatcherActorComponent.h"
#include "BaseNode.h"
#include "GraphBaseNode.h"
#include "ButtonsPanel.h"

class BaseNode;
class SphereNode;
class FJsonObject;
class AGraphBaseNode;
class UButtonsPanel;

// Sets default values for this component's properties
UObjectMatcherActorComponent::UObjectMatcherActorComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	GraphGameExecutor* GameExecutor = new GraphGameExecutor();
    
    ButtonsPanel = NewObject<UButtonsPanel>();

	GameInstance = GameExecutor->GetGameInstance();
    
    ButtonsPanel->SetGameInstance(GameInstance);
	// ...
}

// Called when the game starts
void UObjectMatcherActorComponent::BeginPlay()
{
	Super::BeginPlay();

	BaseNode::component = this;
    
    GameInstance->SetObjectMatcherActorComponent(this);
	GameInstance->StartGame();
}

// Called every frame
void UObjectMatcherActorComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UObjectMatcherActorComponent::SetHoveredGraphBaseNode(AGraphBaseNode* GraphBaseNode)
{
    HoveredGraphBaseNode = GraphBaseNode;
}

AGraphBaseNode* UObjectMatcherActorComponent::GetHoveredGraphBaseNode()
{
    return HoveredGraphBaseNode;
}

UButtonsPanel* UObjectMatcherActorComponent::GetButtonsPanel()
{
    return ButtonsPanel;
}
