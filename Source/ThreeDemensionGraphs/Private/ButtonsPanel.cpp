// Fill out your copyright notice in the Description page of Project Settings.

#include "ButtonsPanel.h"
#include "ButtonsActions.h"
#include "GraphGameInstance.h"
class ButtonsActions;
class GraphGameInstance;

UButtonsPanel::UButtonsPanel()
{
}

void UButtonsPanel::action1()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnAction1();
    }
}

void UButtonsPanel::action2()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnAction2();
    }
}

void UButtonsPanel::action3()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnAction3();
    }
}

void UButtonsPanel::action4()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnAction4();
    }
}

void UButtonsPanel::action5()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnAction5();
    }
}

void UButtonsPanel::action6()
{
    ButtonsActions* Actions = GameInstance->GetButtonsActions();
    if (Actions) {
        Actions->OnAction6();
    }
}

void UButtonsPanel::SetGameInstance(GraphGameInstance* NewGameInstance)
{
    GameInstance = NewGameInstance;
}
