// Fill out your copyright notice in the Description page of Project Settings.

#include "SphereNode.h"
#include "GraphMeshNode.h"
#include "ObjectMatcherActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/EngineTypes.h"

SphereNode::SphereNode()
{
}

SphereNode::~SphereNode()
{
}

void SphereNode::SetColor(Color newColor)
{
	color = newColor;
    if (graphBaseNode)
    {
        graphBaseNode->color = CastColorToFColor(newColor);
        ((AGraphMeshNode *) graphBaseNode)->OnChangeColorEvent();
    }
}

void SphereNode::Spawn()
{
	UObjectMatcherActorComponent* MyComp = component;
	//MyComp.SphereNode;
	TSubclassOf<AGraphMeshNode> sphereNode = MyComp->sphereNode;
	FTransform SpawnTransform;
	FVector SpawnLocation = CastPositionToFVector(position);
	SpawnTransform.SetLocation(SpawnLocation);
	SpawnTransform.SetRotation(FQuat(0, 0, 0, 0));
	SpawnTransform.SetScale3D(FVector(1, 1, 1));

	AGraphMeshNode* spawnNode = MyComp->GetWorld()->SpawnActorDeferred<AGraphMeshNode>(sphereNode, SpawnTransform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if (spawnNode)
	{
		spawnNode->color = CastColorToFColor(color);
		spawnNode->SetBaseNode(this);
		UGameplayStatics::FinishSpawningActor(spawnNode, SpawnTransform);
        graphBaseNode = spawnNode;
	}
	//MyComp->GetWorld();
}

void SphereNode::SetPosition(Position newPosition)
{
	position = newPosition;
	if (graphBaseNode) {
        FTransform SpawnTransform;
        FVector SpawnLocation = CastPositionToFVector(position);
        SpawnTransform.SetLocation(SpawnLocation);
        SpawnTransform.SetRotation(FQuat(0, 0, 0, 0));
        SpawnTransform.SetScale3D(FVector(1, 1, 1));
        graphBaseNode->SetActorTransform(SpawnTransform, false, nullptr, ETeleportType::None);
	}
}
