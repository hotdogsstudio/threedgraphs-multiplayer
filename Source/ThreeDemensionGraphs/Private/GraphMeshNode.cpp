// Fill out your copyright notice in the Description page of Project Settings.

#include "GraphMeshNode.h"




AGraphMeshNode::AGraphMeshNode()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	MeshComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComp->SetCollisionObjectType(ECC_WorldDynamic);
	MeshComp->SetCollisionResponseToAllChannels(ECR_Ignore);
	MeshComp->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECR_Block);
	RootComponent = MeshComp;

}

void AGraphMeshNode::OnChangeColorEvent_Implementation(){}
