// Fill out your copyright notice in the Description page of Project Settings.

#include "LineNode.h"
#include "Kismet/GameplayStatics.h"
#include "GraphLineNode.h"

LineNode::LineNode()
{
}

LineNode::~LineNode()
{
}

void LineNode::Spawn()
{
	TSubclassOf<AGraphLineNode> lineNode = component->lineNode;
	FTransform SpawnTransform;
	FVector SpawnLocation = CastPositionToFVector(startPosition);
	SpawnTransform.SetLocation(SpawnLocation);
	SpawnTransform.SetRotation(FQuat(0, 0, 0, 0));
	SpawnTransform.SetScale3D(FVector(1, 1, 1));

	AGraphLineNode* spawnNode = component->GetWorld()->SpawnActorDeferred<AGraphLineNode>(lineNode, SpawnTransform, nullptr, nullptr, ESpawnActorCollisionHandlingMethod::AlwaysSpawn);
	if (spawnNode)
	{
		spawnNode->color = CastColorToFColor(color);
		spawnNode->startPosition = CastPositionToFVector(startPosition);
		spawnNode->endPosition = CastPositionToFVector(endPosition);
		spawnNode->SetBaseNode(this);
		UGameplayStatics::FinishSpawningActor(spawnNode, SpawnTransform);
        graphBaseNode = spawnNode;
	}
}

void LineNode::SetColor(Color newColor)
{
	color = newColor;
    if (graphBaseNode)
    {
        graphBaseNode->color = CastColorToFColor(newColor);
    }
}

void LineNode::SetPosition(Position newStartPosition, Position newEndPosition)
{
	startPosition = newStartPosition;
	endPosition = newEndPosition;
    if (graphBaseNode)
    {
        AGraphLineNode* node = (AGraphLineNode*)graphBaseNode;
        node->startPosition = CastPositionToFVector(newStartPosition);
        node->endPosition = CastPositionToFVector(newEndPosition);
        node->OnChangePositionEvent();
    }
}
