// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseNode.h"
#include "UnrealString.h"
#include "GraphBaseNode.generated.h"

class BaseNode;

UCLASS()
class THREEDEMENSIONGRAPHS_API AGraphBaseNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGraphBaseNode();

private:
	BaseNode* baseNode;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	void SetBaseNode(BaseNode* newBaseNode);
    
    BaseNode* GetBaseNode();

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Params")
	FLinearColor color;

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Props")
	FString GetTitle();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Props")
	FString GetContent();
};
