// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphBaseNode.h"
#include "GraphLineNode.generated.h"


class UCapsuleComponent;
class USceneComponent;
/**
 * 
 */
UCLASS()
class THREEDEMENSIONGRAPHS_API AGraphLineNode : public AGraphBaseNode
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Components")
	USceneComponent* Origin;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Components")
	UCapsuleComponent* CapsuleComp;
	
public:

    AGraphLineNode();

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Params")
	FVector startPosition;

	UPROPERTY(Replicated, BlueprintReadOnly, Category = "Params")
	FVector endPosition;
    
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
    void OnChangePositionEvent();
    virtual void OnChangePositionEvent_Implementation();
};
