// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class ButtonsActions
{
public:
	ButtonsActions();
	~ButtonsActions();
    
    virtual void OnAction1();
    virtual void OnAction2();
    virtual void OnAction3();
    virtual void OnAction4();
    virtual void OnAction5();
    virtual void OnAction6();
};
