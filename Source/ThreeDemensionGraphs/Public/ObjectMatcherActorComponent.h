// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <map>
#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CoreTypes.h"
#include <string>
#include "GraphGameExecutor.h"
#include "GraphBaseNode.h"
#include "ObjectMatcherActorComponent.generated.h"

using namespace std;
class AGraphMeshNode;
class AGraphLineNode;
class AGraphBaseNode;
class GraphGameInstance;
class UButtonsPanel;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THREEDEMENSIONGRAPHS_API UObjectMatcherActorComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	GraphGameInstance* GameInstance;
    
    AGraphBaseNode* HoveredGraphBaseNode = nullptr;
    
    UButtonsPanel* ButtonsPanel = nullptr;
public:	
	// Sets default values for this component's properties
	UObjectMatcherActorComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	
	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphMeshNode> sphereNode;

	UPROPERTY(EditDefaultsOnly, Category = "Nodes")
	TSubclassOf<AGraphLineNode> lineNode;
    
    UFUNCTION(BlueprintCallable, Category = "Buttons Panel")
    UButtonsPanel* GetButtonsPanel();
    
    UFUNCTION(BlueprintCallable, Category = "Nodes")
    void SetHoveredGraphBaseNode(AGraphBaseNode* GraphBaseNode);
    
    AGraphBaseNode* GetHoveredGraphBaseNode();
};
