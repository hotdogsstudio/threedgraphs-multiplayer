// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphBaseNode.h"
#include "GraphMeshNode.generated.h"

class UStaticMeshComponent;

/**
 * 
 */
UCLASS()
class THREEDEMENSIONGRAPHS_API AGraphMeshNode : public AGraphBaseNode
{
	GENERATED_BODY()

public:

	AGraphMeshNode();
    
    UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Custom Event")
    void OnChangeColorEvent();
    virtual void OnChangeColorEvent_Implementation();

protected:

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* MeshComp;
};
