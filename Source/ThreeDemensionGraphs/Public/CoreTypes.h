#ifndef CoreTypes_h
#define CoreTypes_h

struct Position {
    float x;
    float y;
    float z;
};

struct Color
{
	float r;
	float g;
	float b;
	float a;
};

#endif /* CoreTypes_h */
