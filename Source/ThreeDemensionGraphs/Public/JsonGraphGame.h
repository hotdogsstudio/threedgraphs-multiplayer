// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphGameInstance.h"
#include "CoreTypes.h"
#include <string>

/**
 * 
 */
class JsonGraphGame : public GraphGameInstance
{
protected:
	Color IntToColor(int intColor);
	std::string FStringToStdString(FString fstring);
public:
	JsonGraphGame();
	~JsonGraphGame();

	void StartGame() override;
};
