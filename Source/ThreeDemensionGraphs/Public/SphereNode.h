// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseNode.h"
#include "CoreTypes.h"

/**
 * 
 */

class SphereNode : public BaseNode
{
private:
	Color color;
	Position position;
public:
	SphereNode();
	~SphereNode();

	void Spawn() override;

	void SetPosition(Position newPosition);
	void SetColor(Color newColor);
};
