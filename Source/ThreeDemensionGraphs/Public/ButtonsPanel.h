// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GraphGameInstance.h"
#include "ButtonsPanel.generated.h"

/**
 * 
 */
UCLASS(ClassGroup=(Custom))
class UButtonsPanel : public UObject
{
    GENERATED_BODY()
private:
    GraphGameInstance* GameInstance = nullptr;
    
public:
	UButtonsPanel();
    
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action1();
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action2();
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action3();
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action4();
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action5();
    UFUNCTION(BlueprintCallable, Category = "Button Actions")
    void action6();
    
    void SetGameInstance(GraphGameInstance* NewGameInstance);
};
