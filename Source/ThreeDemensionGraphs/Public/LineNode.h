// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BaseNode.h"
#include "CoreTypes.h"

/**
 * 
 */
class LineNode : public BaseNode
{
private:
	Color color;
	Position startPosition;
	Position endPosition;
public:
	LineNode();
	~LineNode();

	void Spawn() override;

	void SetColor(Color newColor);
	void SetPosition(Position newStartPosition, Position newEndPosition);
};
