
#pragma once

#include "CoreMinimal.h"
#include "ButtonsActions.h"
#include "JsonGraphGame.h"
class ButtonsActions;
class JsonGraphGame;

/**
 * 
 */
class JsonButtonsActions : public ButtonsActions
{
public:
	JsonButtonsActions();
	~JsonButtonsActions();
    
    void OnAction1() override;
    void OnAction2() override;
    void OnAction3() override;
    void OnAction4() override;
    void OnAction5() override;
    void OnAction6() override;
    
    JsonGraphGame* GraphGame = nullptr;
};
