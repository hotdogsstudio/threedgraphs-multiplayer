// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include <string>
#include "ObjectMatcherActorComponent.h"
#include "GraphBaseNode.h"
#include "CoreTypes.h"

class UObjectMatcherActorComponent;
class AGraphBaseNode;

/**
 * 
 */
class BaseNode
{

private:
	std::string title;
	std::string content;

	

protected:
	AGraphBaseNode* graphBaseNode = nullptr;

	static FLinearColor CastColorToFColor(Color color);
	static FVector CastPositionToFVector(Position position);

public:
	BaseNode();
	virtual ~BaseNode() = 0;

	virtual void Spawn();

	void SetTitle(std::string newTitle);
	void SetContent(std::string newContent);

	std::string GetTitle();
	std::string GetContent();

	static UObjectMatcherActorComponent* component;
};
